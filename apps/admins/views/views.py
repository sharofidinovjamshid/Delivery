from rest_framework import viewsets
from django.contrib.auth import get_user_model

from apps.product.views.service import IsAdminOrReadOnly
from ..models import Branches, About, Contact
from ..serializers import BranchSerializer, AboutSerializer, ContactSerializer

User = get_user_model()


class BranchesViewSet(viewsets.ModelViewSet):
    # permission_classes = [IsAdminOrReadOnly(), ]
    queryset = Branches.objects.all()
    serializer_class = BranchSerializer
    tag = ['branch']


class AboutViewSet(viewsets.ModelViewSet):
    # permission_classes = [IsAdminOrReadOnly(), ]
    queryset = About.objects.all()
    serializer_class = AboutSerializer
    tag = ['about']


class ContactViewSet(viewsets.ModelViewSet):
    # permission_classes = [IsAdminOrReadOnly(), ]
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    tag = ['contact']
