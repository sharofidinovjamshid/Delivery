from rest_framework import serializers

from apps.product.models import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'category',
            'liter',
            'name_uz',
            'name_ru',
            'name_en',
            'description_uz',
            'description_ru',
            'description_en',
            'image',
            'price',
            'tax_code',
            'warehouse_code',
            'branches',
            'related_category',
            'is_there'
        ]

        extra_fields = {
            'branches': {'required': False},
            'related_category': {'required': False}
        }

