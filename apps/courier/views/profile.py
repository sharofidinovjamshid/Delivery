from rest_framework import viewsets, status
from rest_framework.response import Response

from apps.supplier.models import SupplierProfile
from apps.supplier.seializers.profile import SupplierProfileSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = SupplierProfile.objects.all()
    serializer_class = SupplierProfileSerializer()

    def list(self, request, *args, **kwargs):
        return

    def retrieve(self, request, *args, **kwargs):
        try:
            profile = self.queryset.objects.get(user=request.user)
            serializer = SupplierProfileSerializer(profile)
            return Response(serializer.data, status.HTTP_200_OK)
        except:
            raise Response({"Error": "Profile Not Found"}, status.HTTP_404_NOT_FOUND)

