from django.contrib.auth import get_user_model
from rest_framework import viewsets, status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, SAFE_METHODS
from rest_framework.response import Response

from apps.admins.models import AdminProfile
from apps.admins.permissions import IsSuperUser
from apps.admins.serializers import (UserModelListSerializer, AdminUserFullSerializer, AdminCreateSerializer,
                                     AdminProfileSerializer)

User = get_user_model()


class AdminUserManager(viewsets.ModelViewSet):
    # permission_classes = [IsAuthenticated, IsSuperUser]
    queryset = User.objects.filter(user_type='admin')
    lookup_field = "email"
    lookup_value_regex = "[^/]+"
    tag = ['admin user manager'.title()]

    def get_serializer_class(self):
        if self.action == 'list':
            return UserModelListSerializer
        elif self.action == 'retrieve':
            return AdminUserFullSerializer
        return AdminCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer_class()
        serializer = serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            admin = User.objects.create(
                email=serializer.validated_data['email'],
                user_type='admin',
                is_superuser=serializer.validated_data['is_superuser'],
                is_staff=True
            )
            AdminProfile.objects.create(
                user=admin,
                first_name=serializer.validated_data['first_name'],
                last_name=serializer.validated_data['last_name'],
                phone_number=serializer.validated_data['phone_number'],
            )
            return Response(serializer.data, status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        profile = AdminProfile.objects.get(user__id=instance.id)
        serializer = self.get_serializer_class()
        serializer = serializer(instance)
        profile_serializer = AdminProfileSerializer(profile)
        return Response({"admin": serializer.data, "profile": profile_serializer.data}, status.HTTP_200_OK)


class CustomerUserManager(viewsets.ModelViewSet):
    # permission_classes = [IsAuthenticated, IsSuperUser]
    queryset = User.objects.filter(user_type='customer')


class SupplierUsermanager(viewsets.ModelViewSet):
    # permission_classes = [IsAuthenticated, IsSuperUser]
    queryset = User.objects.filter(user_type='supplier')
