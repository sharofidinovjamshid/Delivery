from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.admins.models.profile import AdminProfile

User = get_user_model()


class UserModelListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email']


class AdminProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminProfile
        fields = ['id', 'first_name', 'last_name', 'phone_number', 'image']


class AdminCreateSerializer(serializers.Serializer):
    email = serializers.EmailField()
    first_name = serializers.CharField(max_length=100)
    last_name = serializers.CharField(max_length=100)
    phone_number = serializers.CharField(max_length=13)
    is_superuser = serializers.BooleanField(default=False)

    class Meta:
        fields = ['email', 'first_name', 'last_name', 'phone_number']

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError({"Error": "This Email Already Registered"})
        else:
            return value


class AdminUserFullSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'date_joined', 'last_login', 'user_type', 'is_staff']

    # def to_representation(self, instance):
    #     data = super(AdminUserFullSerializer, self).to_representation(instance)
    #     data['profile'] = instance.admin_profile
    #     return data
