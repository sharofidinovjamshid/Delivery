from rest_framework import serializers
from ..models import Basket


class BasketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Basket
        fields = ['id', 'user', 'product', 'quantity', 'ordered']

        extra_kwargs = {
            "user": {'required': False},
            'ordered': {'required': False}
        }

