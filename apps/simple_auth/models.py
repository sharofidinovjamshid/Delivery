from django.db import models
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class CustomUserManager(BaseUserManager):

    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError(_('The Email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):

        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('user_type', 'superuser')

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, password, **extra_fields)


class Users(AbstractUser):
    class UserType(models.TextChoices):
        Superuser = 'superuser'
        Admin = 'admin'
        Supplier = 'supplier'
        Customer = 'customer'

    username = None
    first_name = None
    last_name = None
    email = models.EmailField(max_length=254, unique=True, db_index=True)
    user_type = models.CharField(max_length=25, choices=UserType, default=UserType.Customer)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS =[]
    objects = CustomUserManager()

    def __str__(self):
        return self.email





