from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView

from ..permissions import IsSuperUser
from ..serializers import UserModelListSerializer

User = get_user_model()


class LoginAPIView(TokenObtainPairView):
    queryset = User.objects.all()
    serializer_class = TokenObtainPairSerializer
    tag = ['super admin login'.title()]



