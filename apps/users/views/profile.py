from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import viewsets, permissions, status, mixins
from django.contrib.auth import get_user_model

from ..models import CustomerProfile
from ..serializers import CustomerProfileSerializer

User = get_user_model()


class ProfileView(mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  viewsets.GenericViewSet):

    queryset = CustomerProfile.objects.all()
    serializer_class = CustomerProfileSerializer
    permission_classes = (permissions.IsAuthenticated,)
    tag = ['customer user profile']

    def list(self, request, *args, **kwargs):
        profile = get_object_or_404(self.queryset, user=request.user)
        serializer = self.serializer_class(profile)
        return Response(serializer.data, status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data['user'] = request.user
        serializer.save()
        return Response(serializer.data, status.HTTP_201_CREATED)





