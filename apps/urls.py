from django.urls import path, include

urlpatterns = [
    path('auth/', include('apps.simple_auth.urls'), name='auth'),
    path('admin/', include('apps.admins.urls'), name='admin'),
    path('user/', include('apps.users.urls'), name='user'),
    path('product/', include('apps.product.urls'), name='product'),
]
