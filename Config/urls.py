from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator
from drf_yasg.views import get_schema_view
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import permissions
from rest_framework_simplejwt.views import TokenBlacklistView


class JWTSchemaGenerator(OpenAPISchemaGenerator):

    def get_security_definitions(self):
        security_definitions = super().get_security_definitions()
        security_definitions['Bearer'] = {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        }
        return security_definitions


# class CustomOpenAPISchemaGenerator(OpenAPISchemaGenerator):
#     def get_schema(self, request=None, public=False):
#         """Generate a :class:`.Swagger` object with custom tags"""
#
#         swagger = super().get_schema(request, public)
#         swagger.tags = [
#             {
#                 "name": "api",
#                 "description": "everything about your API"
#             },
#             {
#                 "name": "users",
#                 "description": "everything about your users"
#             },
#         ]
#
#         return swagger


schema_view = get_schema_view(

    openapi.Info(
        title="API",
        default_version='v1',
        description="Delivery",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="sharofidinovjamshid@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
    generator_class=JWTSchemaGenerator,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    # AUTH
    path('api/v1/', include('apps.urls'), name='apps'),
    path('logout/', TokenBlacklistView.as_view(), name='token_blacklist'),
    # END AUTH
    path('', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path("__debug__/", include("debug_toolbar.urls")),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
