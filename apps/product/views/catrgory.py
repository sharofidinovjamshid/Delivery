from rest_framework import generics, viewsets, permissions

from apps.product.serializers import GetCategorySerializer, PostCategorySerializer
from apps.product.models import Category
from apps.product.views.service import IsAdminOrReadOnly


class CategoryViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminOrReadOnly, ]
    queryset = Category.objects.all()
    tag = ['Category']

    def get_serializer_class(self):
        if self.request.method in permissions.SAFE_METHODS:
            return GetCategorySerializer
        return PostCategorySerializer

