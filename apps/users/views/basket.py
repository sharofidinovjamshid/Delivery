from django.contrib.auth import get_user_model
from rest_framework import viewsets, status, permissions
from rest_framework.response import Response

from apps.users.models.basket import Basket
from apps.users.serializers.basket import BasketSerializer

User = get_user_model()


class BasketViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    queryset = Basket.objects.all()
    serializer_class = BasketSerializer
    tag = ['Customer Basket']

    def list(self, request, *args, **kwargs):
        baskets = self.queryset.filter(user=request.user, ordered=False)
        serializer = self.serializer_class(baskets, many=True)
        return Response(serializer.data, status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data['user'] = request.user
        serializer.save()
        return Response(serializer.data, status.HTTP_201_CREATED)








