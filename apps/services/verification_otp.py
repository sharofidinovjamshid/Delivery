from django.contrib.auth import get_user_model
from django.core.cache import cache

User = get_user_model()


def verification(email, vrf_code):
    otp = cache.get(email)
    if str(otp) == str(vrf_code):
        return True
    return False
