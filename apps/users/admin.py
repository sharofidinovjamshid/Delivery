from django.contrib import admin
from apps.users.models import Basket, CustomerProfile

admin.site.register(Basket)
admin.site.register(CustomerProfile)
