from django.contrib import admin

from apps.admins.models import AdminProfile, Branches

# Register your models here.
admin.site.register(AdminProfile)
admin.site.register(Branches)