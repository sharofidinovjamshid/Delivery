from django.contrib.auth import get_user_model
from django.db import models

from apps.admins.models import Branches

User = get_user_model()


class CourierProfile(models.Model):
    class Pause(models.TextChoices):
        Lunch = 'Tushlik'
        Prayer = 'Namoz'

    user = models.ForeignKey(User, models.CASCADE, 'courier_profile')
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    image = models.ImageField(upload_to='courier', default='default_avatar.jpg')
    branch = models.ForeignKey(Branches, models.DO_NOTHING)
    pause = models.CharField(max_length=30, choices=Pause, null=True, blank=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.first_name


class Transport(models.Model):
    class TransportType(models.TextChoices):
        OnFoot = "Piyoda"
        Bicycle = "Velosiped"
        Scooter = "Skuter"
        Car = "Mashina"

    driver = models.ForeignKey(User, models.DO_NOTHING)
    type = models.CharField(max_length=30, choices=TransportType)
    model = models.CharField(max_length=150, null=True, blank=True)
    number = models.CharField(max_length=30, null=True, blank=True)
