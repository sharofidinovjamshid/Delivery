from rest_framework import serializers

from apps.users.models import Address


class CustomerAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = [
            'id',
            'user',
            'address',
            'latitude',
            'longitude',
            'apartment',
            'entrance',
            'floor'
        ]

        extra_kwargs = {
            'user': {'required': False},
            'apartment': {'required': False},
            'entrance': {'required': False},
            'floor': {'required': False},
        }
