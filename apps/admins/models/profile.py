from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class AdminProfile(models.Model):
    user = models.ForeignKey(User, models.CASCADE, 'admin_profile')
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=13)
    image = models.ImageField(upload_to='admin', default='default_avatar.jpg')

    def __str__(self):
        return self.first_name
