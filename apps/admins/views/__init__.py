from .admin import LoginAPIView
from .views import BranchesViewSet, ContactViewSet, AboutViewSet
from .user import AdminUserManager