from rest_framework import serializers

from apps.product.models import Category


class GetCategorySerializer(serializers.ModelSerializer):
    # parent = serializers.PrimaryKeyRelatedField(queryset=Category.objects.all(), required=False)

    class Meta:
        model = Category
        fields = ['id', 'name_uz', 'name_ru', 'name_en', 'parent', 'slug']


class PostCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name_uz', 'name_ru', 'name_en', 'parent']

        extra_kwargs = {
            'parent': {'required': False}
        }

