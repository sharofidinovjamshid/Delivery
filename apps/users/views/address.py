from django.shortcuts import get_list_or_404, get_object_or_404
from rest_framework import viewsets, status
from rest_framework.response import Response

from apps.users.serializers import CustomerAddressSerializer
from apps.users.models import Address


class CustomerAddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.all()
    serializer_class = CustomerAddressSerializer
    tag = ['Customer Address']

    def list(self, request, *args, **kwargs):
        serializer = self.serializer_class(Address.objects.filter(user=request.user), many=True)
        return Response(serializer.data, status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data['user'] = request.user
        serializer.save()
        return Response(serializer.data, status.HTTP_201_CREATED)
