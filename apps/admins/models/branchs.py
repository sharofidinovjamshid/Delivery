from django.db import models
from rest_framework import serializers


class Branches(models.Model):
    title = models.CharField(max_length=250)
    address = models.TextField(null=True, blank=True)
    latitude = models.CharField(max_length=150)
    longitude = models.CharField(max_length=150)
    start_time = models.TimeField()
    end_time = models.TimeField()

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.address
