import random

from django.core.mail import send_mail


def send_otp(email):
    otp = random.randint(100_000, 1_000_000)
    print(otp)
    send_mail(
        subject="Delivery: sizga birmartalik tasdiqlash kodini yubordik",
        message=f"Sizning tasdiqlash kodinggiz: {otp}",
        from_email='sharofidinovjamshid@gmail.com',
        recipient_list=[email],
        fail_silently=False
    )
    return otp
