from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import (LoginAPIView, BranchesViewSet, ContactViewSet, AboutViewSet,
                    AdminUserManager,)

router = DefaultRouter()
router.register(r'branch', BranchesViewSet, basename='branch-manger')
router.register(r'admin-manager', AdminUserManager, 'admin-manager')

urlpatterns = [
    path('login/', LoginAPIView.as_view(), name='login'),
    path('', include(router.urls), name='user-manager'),
]
