from rest_framework import viewsets, parsers, mixins, status, filters
from rest_framework.response import Response

from apps.product.models import Product
from apps.product.serializers import ProductSerializer

from apps.product.views.service import IsAdminOrReadOnly
from django_filters import rest_framework as django_filters
from apps.product.filters import ProductFilter


class ProductViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminOrReadOnly, ]
    parser_classes = [parsers.MultiPartParser]
    queryset = Product.objects.all().select_related('category')
    serializer_class = ProductSerializer
    filter_backends = (django_filters.DjangoFilterBackend,)
    filterset_class = ProductFilter
    tag = ['Product']

    def list(self, request, *args, **kwargs):
        category = request.query_params.get('category', None)
        if category:
            self.queryset = self.queryset.filter(category__slug=category)
            return Response(self.serializer_class(self.queryset, many=True).data, status.HTTP_200_OK)
        return super().list(request, *args, **kwargs)
