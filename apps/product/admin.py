from django.contrib import admin

from .models import Category, Product


class CategoryModelAdmin(admin.ModelAdmin):
    fields = ['name_uz', 'name_ru', 'name_en', 'parent', 'slug']
    list_display = ['id', 'name_uz']
    # exclude = ['slug']


class ProductModelAdmin(admin.ModelAdmin):
    list_display = ['id', 'name_uz']


admin.site.register(Category, CategoryModelAdmin)
admin.site.register(Product, ProductModelAdmin)