from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.supplier.models import SupplierProfile, Transport


class SupplierProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = SupplierProfile
        fields = ['user', 'image', 'branch', 'pause', 'status']


class TransportSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['driver', 'type', 'model', 'number']


