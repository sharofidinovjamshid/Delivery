from rest_framework import serializers
from django.contrib.auth import get_user_model

from apps.users.models import CustomerProfile


class CustomerProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerProfile
        fields = ['id', 'user', 'full_name', 'phone_number']

        extra_kwargs = {"user": {"required": False}}