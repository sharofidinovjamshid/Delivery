from django.urls import path, include
from rest_framework_simplejwt.views import TokenRefreshView
from . import views


urlpatterns = [
    path('login/', views.AuthenticationView.as_view({'post': 'send_sms'}), name='send_email'),
    path('verify-code/', views.AuthenticationView.as_view({'post': 'verify_otp'}), name='verify-code'),
    path('verify-code/<str:user_type>/', views.AuthenticationView.as_view({'post': 'verify_otp'}), name='verify-code'),
    path('token-refresh/', TokenRefreshView().as_view(), name='token-refresh'),
]
