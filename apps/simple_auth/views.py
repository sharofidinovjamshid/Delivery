import random
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from drf_yasg.utils import swagger_auto_schema

from django.core.cache import cache
from django.contrib.auth import get_user_model
from .serializers import EmailSerializer, VerificationEmailSerializer
from .service import send_otp

User = get_user_model()


class AuthenticationView(viewsets.ViewSet):

    @swagger_auto_schema(request_body=EmailSerializer, tags=['Auth'])
    def send_sms(self, request):
        serializer = EmailSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):

            email = serializer.validated_data['email']
            verification_code = random.randint(100_000, 1_000_000)
            if send_otp(email=email, otp=verification_code):
                cache.set(email, verification_code, 300)
                return Response({"message": "SMS sent successfully"}, status=status.HTTP_200_OK)
            raise Exception({"message": "Send OTP Failed"})

        return Response(serializer.errors)

    @swagger_auto_schema(request_body=VerificationEmailSerializer, tags=['Auth'])
    def verify_otp(self, request, **kwargs):
        serializer = VerificationEmailSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            email = serializer.validated_data['email']
            cache_code = cache.get(email)
            vrf_code = serializer.validated_data['verification_code']
            print(serializer.validated_data['verification_code'])
            print(cache_code)
            if str(cache_code) == str(vrf_code):
                user, create = User.objects.get_or_create(email=email)
                if create:
                    status_code = status.HTTP_201_CREATED
                    if 'user_type' in kwargs:
                        user_type = kwargs['user_type']
                        user.user_type = user_type
                    user.save()
                else:
                    status_code = status.HTTP_200_OK
                token = RefreshToken.for_user(user)
                return Response({"refresh": str(token), "access": str(token.access_token)}, status=status_code)

            return Response({"message": "Invalid verification code"}, status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
